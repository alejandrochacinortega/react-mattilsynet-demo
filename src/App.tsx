import React, { useState } from "react";
import "./App.css";
import data from "./data.json";
// import { BeerList } from "./components/beer-list";
import { IBeer } from "./types";

interface IBeerListProps {
  beers: IBeer[];
}

interface IBeerProps {
  beer: IBeer;
}

const BeerItem = ({ beer }: IBeerProps) => {
  return (
    <li key={beer.id} className="list-item">
      <div className="list-item--img">
        <img src={beer.image_url} alt="beer" />
      </div>
      <div className="list-item--info">
        <p>{beer.name}</p>
        <ul>
          <li>
            <small>ABV: {beer.abv}</small>
          </li>
          <li>
            <small>
              Volume: {beer.volume.unit} {beer.volume.unit}
            </small>
          </li>
        </ul>
      </div>
    </li>
  );
};

const BeerList = ({ beers }: IBeerListProps) => {
  console.log("beers list ", beers);
  return (
    <ul className="list">
      {beers.map((beer: IBeer) => {
        return <BeerItem beer={beer} key={beer.id} />;
      })}
    </ul>
  );
};

function App() {
  const [searchValue, setSearchValue] = useState("");
  return (
    <div className="container">
      <input
        value={searchValue}
        onChange={(event) => {
          setSearchValue(event.target.value);
        }}
        placeholder="Søk"
      />
      <BeerList beers={data} />
    </div>
  );

  // const [searchValue, setSearchValue] = useState("");

  // const onSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
  //   setSearchValue(event.target.value);
  // };

  // return (
  //   <div className="container">
  //     <div className="input-container">
  //       <input value={searchValue} onChange={onSearch} placeholder="Søk" />
  //     </div>

  //     <BeerList
  //       beers={data.filter(
  //         (beer: IBeer) => !!beer.name.toLowerCase().includes(searchValue)
  //       )}
  //     />
  //   </div>
  // );
}

export default App;
