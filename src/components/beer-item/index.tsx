import React from "react";
import { IBeer } from "../../types";

interface IBeerItemProps {
  beer: IBeer;
}

export const BeerItem = ({ beer }: IBeerItemProps) => {
  return (
    <li key={beer.id} className="list-item">
      <div className="list-item--img">
        <img src={beer.image_url} alt="beer" />
      </div>
      <div className="list-item--info">
        <p>{beer.name}</p>
        <ul>
          <li>
            <small>ABV: {beer.abv}</small>
          </li>
          <li>
            <small>
              Volume: {beer.volume.unit} {beer.volume.unit}
            </small>
          </li>
        </ul>
      </div>
    </li>
  );
};
