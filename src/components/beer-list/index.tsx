import React from "react";

import { IBeer } from "../../types";
import { BeerItem } from "../beer-item";

interface IBeerListProps {
  beers: IBeer[];
}

export const BeerList = ({ beers }: IBeerListProps) => {
  return (
    <ul className="list">
      {beers.map((beer: IBeer) => {
        return <BeerItem key={beer.id} beer={beer} />;
      })}
    </ul>
  );
};
