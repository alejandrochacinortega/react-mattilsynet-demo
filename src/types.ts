export interface IBeer {
  abv: number;
  attenuation_level: number;
  boil_volume: any;
  brewers_tips: string;
  contributed_by: string;
  description: string;
  ebc: number;
  first_brewed: string;
  food_pairing: any;
  ibu: number;
  id: number;
  image_url: string;
  ingredients: any;
  method: any;
  name: string;
  ph: number;
  srm: number;
  tagline: string;
  target_fg: number;
  target_og: number;
  volume: {
    value: number;
    unit: string;
  };
}
