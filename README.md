## Project Instructions ✅

React Mattilsynet Demo

## Getting started

- `yarn install`
- `yarn start`

Open `http://localhost:3000`

### Development Environment and Architecture

- TypeScript
- React

### API and JS Integration

- We are using an array of beers from `https://punkapi.com/` located in `data.json`

### Sessions

#### 1.ReactJS 🤖

- React Hooks (using TypeScript) ✅
- React Skeleton ✅
- React Components ✅
- React Demo using data from `https://punkapi.com/` ✅
  - Create Beer List component ✅
  - Create Beer Item component ✅
  - Create Search Input component ✅
    - React Hooks (useState, useEffect)

#### 2.Redux and Friends 🦾

- Why Redux?
- Add Redux, Redux observableP, RxJs
- React Skeleton
- React Component
- React Hooks (useState, useEffect)

#### 3.CSS 💅

- Grid vs Flex
- Grid
- Flex
- Styled Components
- Use in our projects
  - Grid and Friends
  - TextArea
  - TextLink
  - Header

##### Learn More 📚

- React https://create-react-app.dev/docs/adding-typescript/
- React Hooks https://reactjs.org/docs/hooks-effect.html
- Storybook https://storybook.js.org/
